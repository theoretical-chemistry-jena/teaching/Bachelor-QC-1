# Theoretical Chemistry I

This repository holds documents relating to the first part of the B. Sc. practical course in theoretical chemistry for chemists.

## Mathematica

These notebooks are unfortunately in a rather sorry state, missing some exercises and lacking explanation.
If you, dear reader, are unfortunate enough to need them, please leave them in a better state for benefit of the next poor soul.

Note that while Mathematica is commercial, closed source software, there is a free [Wolfram Player](wolfram.com/player) that can at least read notebooks.

## Quantum Chemistry feat. Gaussian

TBD!
